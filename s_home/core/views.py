from django.views.generic.base import View
from rest_framework import viewsets
from django.http.response import JsonResponse
from . import models
from . import serializers
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.schemas import SchemaGenerator
from rest_framework.views import APIView
from rest_framework_swagger import renderers


class SwaggerSchemaView(APIView):
    permission_classes = [AllowAny]
    renderer_classes = [
        renderers.OpenAPIRenderer,
        renderers.SwaggerUIRenderer
    ]

    def get(self, request):
        generator = SchemaGenerator()
        schema = generator.get_schema(request=request)
        return Response(schema)


class BaseUserViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.BaseUserSerializer
    queryset = models.BaseUserProfile.objects.all()

    def create(self, request, *args, **kwargs):
        _data = {
            "phone": request.data.get("phone", None)
        }
        if not _data["phone"]:
            return JsonResponse({"success": False, "error": "expected data was not received"})
        base_user = models.BaseUserProfile.objects.filter(phone=_data["phone"]).first()
        if base_user:
            return JsonResponse({
                'success': False
            })
        base_user = models.BaseUserProfile(**_data)
        base_user.send_sms()
        base_user.generate_token()
        base_user.save()

        return JsonResponse({
            'success': True
        })


class UserProfileViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.UserSerializer
    queryset = models.UserProfile.objects.all()
    DATA_SET = ["date_of_birth", "first_name", "last_name", "patronymic", "address", "number_room"]

    def list(self, request, *args, **kwargs):
        authorization = request.META.get('HTTP_AUTHORIZATION', None)
        if authorization:
            base_user = models.BaseUserProfile.objects.filter(authorization=authorization).first()
            if base_user:
                user = models.UserProfile.objects.filter(user=base_user).first()
            else:
                return JsonResponse({
                    "error": "user not found"
                })
        else:
            return JsonResponse({
                "error": "authorization failed"
            })

        return JsonResponse({
            "id": user.pk,
            "phone": base_user.phone,
            "date_of_birth": user.date_of_birth,
            "first_name": user.first_name,
            "last_name": user.last_name,
            "patronymic": user.patronymic,
            "address": user.address,
            "number_room": user.number_room
        })

    def retrieve(self, request, *args, **kwargs):
        return JsonResponse({"error": "you do not have access rights"})

    def destroy(self, request, *args, **kwargs):
        return JsonResponse({"error": "you do not have access rights"})

    def validate_data(self, request):
        data = [body_key for body_key in request.data if body_key in self.DATA_SET]
        data_for_instance = {}
        for key_for_instance in data:
            data_for_instance[key_for_instance] = request.data[key_for_instance]

        return data_for_instance

    def update(self, request, *args, **kwargs):
        authorization = request.META.get('HTTP_AUTHORIZATION', None)
        if not authorization:
            return JsonResponse({"success": False, "error": "AuthError"})
        base_user = models.BaseUserProfile.objects.filter(authorization=authorization).first()
        if base_user:
            user = models.UserProfile.objects.filter(user=base_user).first()
            if user.pk != int(kwargs["pk"]):
                return JsonResponse({"error": "ValidateError"})
            base_user.save()

            data_for_response = self.validate_data(request)
            user.date_of_birth = data_for_response.get('date_of_birth', user.date_of_birth)
            user.first_name = data_for_response.get('first_name', user.first_name)
            user.last_name = data_for_response.get('last_name', user.last_name)
            user.patronymic = data_for_response.get('patronymic', user.patronymic)
            user.address = data_for_response.get('address', user.address)
            user.number_room = data_for_response.get('number_room', user.number_room)
            user.save()

            data_for_response["id"] = user.pk
            response = JsonResponse(data_for_response)

            return response

        return JsonResponse({"error": "AuthError"})

    def validate_of_null_from_request(self, request):
        for validate_field in self.DATA_SET:
            if not request.data.get(validate_field, None):
                return False

        return True

    def create(self, request, *args, **kwargs):
        _data = {
            "phone": request.data.get("phone", None)
        }
        if not _data["phone"]:
            return JsonResponse({"success": False, "error": "expected data was not received"})
        base_user = models.BaseUserProfile.objects.filter(phone=_data["phone"]).first()

        if not self.validate_of_null_from_request(request):
            return JsonResponse({"error": "filed not on data"})

        data = {
            "user": base_user,
            "date_of_birth": request.data["date_of_birth"],
            "first_name": request.data["first_name"],
            "last_name": request.data["last_name"],
            "patronymic": request.data["patronymic"],
            "address": request.data["address"],
            "number_room": request.data["number_room"]
        }

        instance = models.UserProfile(**data)
        instance.save()

        data["phone"] = request.data["phone"]
        data["user"] = instance.pk
        response = JsonResponse(data)
        response["authorization"] = base_user.authorization

        return response


class LoginViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.LoginSerializer
    queryset = models.BaseUserProfile.objects.all()

    def list(self, request, *args, **kwargs):
        phone = request.query_params.get("phone", None)
        if phone:
            _validate = models.BaseUserProfile.objects.filter(phone=phone).first()
            if _validate:
                return JsonResponse({"success": True})
            else:
                return JsonResponse({"success": False})

        return JsonResponse({"success": False, "error": "expected data was not received"})

    def retrieve(self, request, *args, **kwargs):
        return JsonResponse({"error": "you do not have access rights"})

    def destroy(self, request, *args, **kwargs):
        return JsonResponse({"error": "you do not have access rights"})

    def update(self, request, *args, **kwargs):
        return JsonResponse({"error": "you do not have access rights"})

    def create(self, request, *args, **kwargs):
        try:
            base_user = models.BaseUserProfile.objects.filter(phone=request.data["phone"]).first()
            if base_user:
                user = models.UserProfile.objects.filter(user=base_user).first()
            else:
                return JsonResponse({
                    "error": "user not found"
                })
        except KeyError:
            return JsonResponse({
                "error": "data not found"
            })

        response = JsonResponse({
            "id": user.pk,
            "phone": base_user.phone,
            "date_of_birth": user.date_of_birth,
            "first_name": user.first_name,
            "last_name": user.last_name,
            "patronymic": user.patronymic,
            "address": user.address,
            "number_room": user.number_room
        })
        base_user.save()

        return response


class ResidentialComplexesViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.ResidentialComplexesSerializer
    queryset = models.ResidentialComplexes.objects.all()

    def make_struct(self, data):
        return {
            'id': data["id"],
            'name': data["name"],
            'address': data["address"]
        }

    def list(self, request, *args, **kwargs):
        _data = super().list(request)
        if _data:
            return JsonResponse({"data": [self.make_struct(complex_r) for complex_r in _data.data]})

        return JsonResponse({"data": None})

    def destroy(self, request, *args, **kwargs):
        return JsonResponse({"error": "you do not have access rights"})

    def update(self, request, *args, **kwargs):
        return JsonResponse({"error": "you do not have access rights"})


class CamerasViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.CamerasSerializer
    queryset = models.Cameras.objects.all()
    BASE_URL = "http://185.86.145.54:86/api/complex"

    def make_struct(self, data):
        res_complex: int = data["res_complex"]
        _complex = models.ResidentialComplexes.objects.filter(id=res_complex).first()
        return {
            'id': data["id"],
            'name': _complex.name,
            'address': _complex.address,
            'link': data["link"],
            "res_complex": "{}/{}/".format(self.BASE_URL, res_complex),
            'info': data["info"],
        }

    def make_target_struct(self, data):
        res_complex: int = data.res_complex.id
        _complex = models.ResidentialComplexes.objects.filter(id=res_complex).first()
        return {
            'id': data.pk,
            'name': _complex.name,
            'address': _complex.address,
            'link': data.link,
            "res_complex": "{}/{}/".format(self.BASE_URL, res_complex),
            'info': data.info,
        }

    def list(self, request, *args, **kwargs):
        try:
            _complex = models.ResidentialComplexes.objects.filter(id=int(request.query_params["res_complex"])).first()
            if _complex:
                data = models.Cameras.objects.filter(res_complex=_complex).all()
            else:
                data = None
            if data:
                cameras = [self.make_target_struct(cam) for cam in data]
                return JsonResponse({"data": cameras})

        except KeyError:
            _data = super().list(request)
            if _data:
                return JsonResponse({"data": [self.make_struct(cam) for cam in _data.data]})

        return JsonResponse({"data": None})

    def retrieve(self, request, *args, **kwargs):
        return JsonResponse({"error": "you do not have access rights"})

    def destroy(self, request, *args, **kwargs):
        return JsonResponse({"error": "you do not have access rights"})

    def update(self, request, *args, **kwargs):
        return JsonResponse({"error": "you do not have access rights"})


class AuthUpdateView(View):

    def post(self, request, *args, **kwargs):
        try:
            auth = request.META['HTTP_AUTHORIZATION']
            user = models.BaseUserProfile.objects.filter(phone=request.data["phone"]).first()
        except KeyError:
            user = None

        if user:
            user.authorization = auth
            user.save()

            return JsonResponse({
                "success": True
            })

        return JsonResponse({
            "success": False
        })


class ValidateView(View):
    def post(self, request):
        phone = request.data.get("phone", None)
        _code = request.data.get("validate_code", None)
        if not phone or not _code:
            return JsonResponse({"error": "expected field phone or validate_code"})
        base_user = models.BaseUserProfile.objects.filter(phone=phone).first()
        if base_user:
            if base_user.validate_code == _code:
                base_user.is_active = True
                user = models.UserProfile.objects.filter(user=base_user).first()

                response = JsonResponse({
                    "id": user.pk,
                    "phone": base_user.phone,
                    "date_of_birth": user.date_of_birth,
                    "first_name": user.first_name,
                    "last_name": user.last_name,
                    "patronymic": user.patronymic,
                    "address": user.address,
                    "number_room": user.number_room
                })
                user.save()
                response["authorization"] = base_user.authorization

                return JsonResponse({"success": True, "data": response})
            return JsonResponse({"success": False, "error": "validate_code not validated"})
        return JsonResponse({"success": False, "error": "user not found"})


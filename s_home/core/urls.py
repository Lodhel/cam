from django.conf.urls import url, include
from drf_yasg import openapi
from drf_yasg.views import get_schema_view

from rest_framework.routers import DefaultRouter
from . import views

router = DefaultRouter()
router.register('cameras', views.CamerasViewSet)
router.register('complex', views.ResidentialComplexesViewSet)
router.register('auth/profile', views.UserProfileViewSet)
router.register('auth/login', views.LoginViewSet)
router.register('auth/validate_phone', views.BaseUserViewSet)


urlpatterns = [
    url(r'swagger/', views.SwaggerSchemaView().as_view()),
    url(r'^auth/update/$', views.AuthUpdateView.as_view(), name='auth_update'),
    url(r'^auth/validate_code/$', views.ValidateView.as_view()),
    url(r'', include(router.urls)),
]

openapi_info = openapi.Info(
    title="Cameras API",
    default_version='v1',
    description="API to access Cameras",
    terms_of_service="https://github.com/",
    license=openapi.License(name="Nginx"),
)
schema_view = get_schema_view(
    openapi_info,
    validators=['flex', 'ssv'],
    public=True,
)

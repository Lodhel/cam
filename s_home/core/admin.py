from django.contrib import admin
from .models import *


class AdminProfile(admin.ModelAdmin):
    search_fields = ('user', )


class AdminBaseProfile(admin.ModelAdmin):
    search_fields = ('phone', )


# Register your models here.
admin.site.register(ResidentialComplexes)
admin.site.register(Cameras)
admin.site.register(UserProfile, AdminProfile)
admin.site.register(BaseUserProfile, AdminBaseProfile)
import os
import firebase_admin
from firebase_admin import credentials, firestore, storage


class FirebaseMixin:
    CRED_FILE: str = "{}/credentials.json".format(os.path.dirname(os.path.abspath(__file__)))

    def create(self):
        return credentials.Certificate(self.CRED_FILE)


class FireBaseDataService(FirebaseMixin):

    def init_db(self):
        if not firebase_admin._apps:
            firebase_admin.initialize_app(self.create())
        return firestore.client()

    def push_data(self, data):
        doc_ref = self.init_db().collection(self.collect).document(self.doc)
        doc_ref.set(data)
        
    def get_data(self, collection):
        data = []
        docs = self.init_db().collection(collection).stream()

        for doc in docs:
            data.append(doc.to_dict())
        
        return data


class FireBaseStorage(FirebaseMixin):

    def init_storage(self):
        if not firebase_admin._apps:
            firebase_admin.initialize_app(self.create(), {'storageBucket': 'mobecan-92f56.appspot.com'})
        bucket = storage.bucket()
        return bucket

    def push_data(self, path_on_cloud, path_local):
        blob = self.init_storage().blob(path_on_cloud)
        blob.upload_from_filename(path_local)

    def get_data(self, path_on_cloud, path_local):
        blob = self.init_storage().blob(path_on_cloud)
        blob.download_to_filename(path_local)
        
import datetime
import random
import string
import uuid

import requests
from django.db import models


class BaseUserProfile(models.Model):
    phone = models.CharField(max_length=32, unique=True, verbose_name="Телефон")
    authorization = models.TextField(verbose_name="Токен")
    validate_code = models.CharField(max_length=8, null=True, verbose_name="Код валидации")
    is_active = models.BooleanField(default=False)

    def __str__(self):
        return str(self.phone)

    def generate_token(self):
        result = str(uuid.uuid4())
        self.authorization = result[0:64]

    def generate_code(self, size=4, chars=string.digits):
        _code = ''.join(random.choice(chars) for _ in range(size))
        self.validate_code = _code
        return _code

    def send_sms(self):
        email = 'ruslansemenko@yandex.ru'
        key = 'LQ3dBvMDJXaGb2lW9AkztcLNzqt9'
        url = 'https://{}:{}@gate.smsaero.ru/v2/sms/send'.format(email, key)

        data = {
            'number': self.phone,
            'text': "Ваш код для подтверждения регистрации на сервисах Kron\r\n {}".format(self.generate_code()),
            'sign': "SMS Aero",
            'channel': 'DIRECT'
        }
        requests.post(url, data)

    class Meta:
        db_table = "base_profile"
        verbose_name = 'Базовый Профиль Пользователя'
        verbose_name_plural = 'Базовые Профили Пользователей'


class UserProfile(models.Model):
    user = models.OneToOneField(BaseUserProfile, on_delete=models.CASCADE, verbose_name="базовый пользователь")
    date_joined = models.DateTimeField(default=datetime.datetime.today())
    date_of_birth = models.DateTimeField(verbose_name="Дата Рождения")
    first_name = models.CharField(max_length=64, verbose_name="Имя")
    last_name = models.CharField(max_length=64, verbose_name="Фамилия")
    address = models.IntegerField(verbose_name="id Адреса")
    patronymic = models.CharField(max_length=64, null=True, verbose_name="Отчество")
    number_room = models.CharField(max_length=16, verbose_name="Номер комнаты")

    def __str__(self):
        return str(self.user)

    class Meta:
        verbose_name = 'Профиль Пользователя'
        verbose_name_plural = 'Профили Пользователей'
        db_table = "profile"


class ResidentialComplexes(models.Model):
    address = models.CharField(max_length=100, verbose_name='Адрес')
    name = models.CharField(max_length=50, verbose_name='Название')
    date_create = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')

    class Meta:
        verbose_name = 'Жилые комплексы'
        verbose_name_plural = 'Жилые комплексы'
        db_table = "residential_complexes"

    def __str__(self):
        return self.name


class Cameras(models.Model):
    info = models.CharField(max_length=100, verbose_name='Описание')
    res_complex = models.ForeignKey(ResidentialComplexes, on_delete=models.CASCADE, null=True,
                                    verbose_name='Жилой комплекс')
    create_date = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    link = models.TextField(verbose_name='Ссылка на поток')

    class Meta:
        db_table = "cameras"
        verbose_name = 'Камеры'
        verbose_name_plural = 'Камеры'

    def __str__(self):
        return self.info

    def get_name(self):
        return str(self.res_complex.name)

    get_name.short_description = 'Название ЖК'

    def get_address(self):
        return str(self.res_complex.address)

    get_address.short_description = 'Адрес ЖК'

from django.db import transaction
from rest_framework import serializers

from . import models


class BaseUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.BaseUserProfile
        fields = (
            'id', 'phone'
        )


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.UserProfile
        fields = (
            'id', 'date_of_birth', "first_name", "last_name", "patronymic",
            "address", "number_room"
        )


class LoginSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.BaseUserProfile
        fields = (
            'id', "phone"
        )
        extra_kwargs = {
            'authorization': {'write_only': True}
        }


class ResidentialComplexesSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ResidentialComplexes
        fields = '__all__'

    @transaction.atomic()
    def create(self, validated_data):
        instance = super().create(validated_data)
        instance.save()
        return instance


class CamerasSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Cameras
        fields = '__all__'

    @transaction.atomic()
    def create(self, validated_data):
        instance = super().create(validated_data)
        instance.save()

        return instance

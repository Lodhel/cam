# Generated by Django 3.1.1 on 2021-05-15 13:02

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_auto_20210512_2024'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='cameras',
            name='address',
        ),
        migrations.RemoveField(
            model_name='cameras',
            name='name',
        ),
        migrations.RemoveField(
            model_name='cameras',
            name='status',
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='date_joined',
            field=models.DateTimeField(default=datetime.datetime(2021, 5, 15, 16, 2, 16, 825195)),
        ),
    ]

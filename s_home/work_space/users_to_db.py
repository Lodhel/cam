import io
import json
import os

import requests

base_dir = "{}/user.json".format(os.path.dirname(os.path.abspath(__file__)))

n = 2081693702729
with io.open(base_dir, encoding='utf-8') as file:
    data = json.load(file)
    for instance in data:
        body = {
            "phone": instance["fields"]["phone"],
            "first_name": instance["fields"]["first_name"],
            "last_name": instance["fields"]["last_name"],
            "patronymic": instance["fields"]["patronymic"],
            "date_of_birth": instance["fields"]["date_of_birth"],
            "address": instance["fields"]["address"],
            "number_room": instance["fields"]["number_room"]
        }
        n += 25
        request = requests.post("http://185.86.145.54:86/api/auth/profile/", json=body, headers={"authorization": str(n)})
        result = request.content
        print(result)
#FROM python:3.9.1-slim-buster as builder
#WORKDIR /s_home
#RUN apt update && apt-get install -y git
#RUN pip install pip==21.1.2
#COPY . .
#RUN ls -l
#RUN pip install -r s_home/requirements.txt

#RUN chmod +x s_home/manage.py
#RUN s_home/manage.py makemigrations
#RUN s_home/manage.py migrate
#
#CMD ["/manage.py", "runserver"]
#
#EXPOSE 8000


FROM python:3.8.3-alpine
# set work directory
WORKDIR /usr/src/app
# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
# install psycopg2 dependencies
RUN apk update \
    && apk add postgresql-dev gcc python3-dev musl-dev
# install dependencies
RUN pip install --upgrade pip
COPY ./s_home/requirements.txt .
RUN pip install -r requirements.txt

COPY ./entrypoint.sh .
RUN chmod +x ./entrypoint.sh

# copy project
COPY ./s_home .

# run entrypoint.sh
#ENTRYPOINT /usr/src/app/entrypoint.sh